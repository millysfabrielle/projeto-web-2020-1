import axios from "axios";

class CarroService{

    constructor(){
        this.connection = axios.create({baseURL: 'http://localhost:3000'});
    }

    findAll(){
        return this.connection.get('/carros');
    }

    findById(id){
        return this.connection.get('/carros/'+id);
    }

    save( carro ){

        if(carro.id){
            return this.connection.put('/carros/'+ carro.id, carro);
        } 

        return this.connection.post('/carros', carro);

    }

    delete( id ){
        return this.connection.delete('/carros/'+id);
    }

}

export default new CarroService();