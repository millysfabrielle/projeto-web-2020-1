import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import {Provider} from "react-redux";
import store from "./redux/store"

import './App.css';
import CarroForm from './views/CarroForm';
import CarroList from './views/CarroList';

function App() {
  return (
    <div>
      <Provider store={store}>

      <Router>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item active">
                  <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Features</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Pricing</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Disabled</a>
                </li>
              </ul>
            </div>
          </nav>

          <div className="row">

            {/* MENU VERTICAL */}
            <div className="col-2">
              <ul className="nav flex-column">
                <li className="nav-item">
                  <Link to="/carro/list">Link com tag Link</Link>
                </li>
              </ul>
            </div>

            {/* CONTEÚDO DINÂMICO */}
            <div className="col-10">
              <Switch>
                  <Route exact path="/carro/list" component={CarroList} />
                  <Route exact path="/carro/form/:id" component={CarroForm} />
                  <Route exact path="/carro/form" component={CarroForm} />
              </Switch>
            </div>
          </div>
        </Router>

      </Provider>
    </div>
  );
}

export default App;
